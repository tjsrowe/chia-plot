#!/bin/bash

if [ -z "$FARMER_PEER_IP" ]; then
	echo "No FARMER_PEER_IP specified."
	exit 2
fi

if [ -z "$FARMER_PEER_PORT" ]; then
	FARMER_PEER=$FARMER_PEER_IP:$DEFAULT_FARMER_PORT
else
	FARMER_PEER=$FARMER_PEER_IP:$FARMER_PEER_PORT
fi

if [ -z "$LOG_LEVEL" ]; then
	LOG_LEVEL=INFO
fi

[ -r /home/chia/plots ] && echo "/home/chia/plots is readable" || (echo "/home/chia/plots is not readable"; exit 3)

if [ ! -f /home/chia/ca/private_ca.crt ]; then
	echo "ca/private_ca.crt not found"
	exit 4
fi

. /common.sh

chia init
chia init -c /home/chia/ca
echo "Setting farmer peer to $FARMER_PEER"
chia configure --set-farmer-peer $FARMER_PEER
chia configure --set-log-level $LOG_LEVEL
if [ ! -d ~/.chia/mainnet/log ]; then
	mkdir -p ~/.chia/mainnet/log/
	touch ~/.chia/mainnet/log/debug.log
fi

shopt -s dotglob
#find /mnt/plot* -prune -type d | while IFS= read -r d; do
#	chia plots add -d $d
#done   
find / -type f -name '*.plot*' 2>/dev/null | sed -r 's|/[^/]+$||' |sort -u | xargs -I{} chia plots add -d {}


chia start harvester

tail -F ~/.chia/mainnet/log/debug.log