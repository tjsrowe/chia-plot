ARG CHIA_VERSION=1.5.0
FROM chia-base:${CHIA_VERSION}

USER root

RUN mkdir /home/chia/tmp && mkdir /home/chia/plots
COPY plot.sh /plot.sh
RUN chown chia:chia /plot.sh && chmod 700 /plot.sh

USER chia

ENV PLOT_SIZE 32
ENTRYPOINT ["/plot.sh"]
