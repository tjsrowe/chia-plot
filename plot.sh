#!/bin/bash

if [ -z "$FARMER_PUBLIC_KEY" ]; then
	echo "No FARMER_PUBLIC_KEY specified."
	exit 1
fi

if [ -z "$POOL_PUBLIC_KEY" ]; then
	echo "No POOL_PUBLIC_KEY specified."
	exit 2
fi

[ -w /home/chia/plots ] && echo "/home/chia/plots is writeable" || (echo "/home/chia/plots is not writeable"; exit 3)
[ -w /home/chia/tmp/ ] && echo "/home/chia/tmp/ is writeable" || (echo "/home/chia/tmp/ is not writeable"; exit 4)

if [ -z "$PLOT_SIZE" ]; then
	PLOT_SIZE=32
fi

. /common.sh

chia init
chia plots create $PLOT_ARGS -k $PLOT_SIZE -t /home/chia/tmp -d /home/chia/plots -f $FARMER_PUBLIC_KEY -p $POOL_PUBLIC_KEY $@