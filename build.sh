#!/bin/bash
CHIA_VERSION=1.5.0
CHIA_UID=1003
CHIA_GID=1003

docker build --build-arg CHIA_UID=$CHIA_UID --build-arg CHIA_GID=$CHIA_GID -f Dockerfile.base -t chia-base:latest .
docker tag chia-base:latest chia-base:$CHIA_VERSION
docker build -f Dockerfile.plot -t chia-plot:latest .
docker tag chia-plot:latest chia-plot:$CHIA_VERSION
docker build -f Dockerfile.harvester -t chia-harvester:latest .
docker tag chia-harvester:latest chia-harvester:$CHIA_VERSION
