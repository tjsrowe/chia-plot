CHIA_VERSION = 1.5.0

base:
	docker build -t chia-base:${CHIA_VERSION} -f Dockerfile.base .

harvester: base
	docker build -t chia-harvester:${CHIA_VERSION} -f Dockerfile.harvester .

all: base harvester