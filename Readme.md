# README #

This is a basic Dockerfile for generating a chai plot
It has a base image which just containers the latest version, and then a dedicated plotter and harvester image which use the /plot.sh and /harvester.sh entrypoint.

### How do I get set up? ###

* Cloning this repo
You'll need the repo locally as Docker will want to build in the .sh files to the container image, which it can't just get from the remote.
git clone https://bitbucket.org/tjsrowe/chia-plot.git
or
git clone git@bitbucket.org:tjsrowe/chia-plot.git

* Build your image
docker build -f Dockerfile.base -t chia-base:1.0.4 .
docker build -f Dockerfile.plot -t chia-plot:1.0.4 .
docker build -f Dockerfile.harvest -t chia-harvest:1.0.4 .

* Running the plotter
docker run --env-file=chia.env --restart=always -d --network host --name chiaharvest -v /mnt/chia:/home/chia/plots -v /mnt/chia-work:/home/chia/tmp -it chia-plot

* Running the harvester
The harvester will require the private_ca.key and private_ca.crt file in order to communicate over SSL with the farmer.  You'll need to get this from your harvesters config.
The only keys you need to be able to init the harvester are the ones from that $CHIA_HOME/.chia/mainnet/config/ssl/ca - you don't require any other parts of your config to get it up and running.  You can mount these in to your container using -v $CHIA_HOME/.chia/mainnet/config/ssl/ca:/home/chia/ca:ro

CHIA_HOME=~chia
docker run --env-file=chia.env --env-file=harvester.env --restart=always -d --network host --name chiaharvest -v /mnt/chiaplot1:/mnt/plot1 -v /mnt/chiaplot2:/mnt/plot2 -v ~chia/.chia/mainnet/config/ssl/ca:/home/chia/ca:ro -it chia-harvest:1.0.4

* Environment
chia.env should contain:
POOL_PUBLIC_KEY
FARMER_PUBLIC_KEY

### Who do I talk to? ###
* Tim Rowe <tim@tjsr.id.au>
